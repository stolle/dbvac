package de.rstolle.dbvac.api;

public enum Nullable {
    YES(true, ""), NO(false, "NOT NULL"), UNKNOWN(null, "");

    public final Boolean nullable;
    public final String sql;

    Nullable(final Boolean nullable, final String sql) {
        this.nullable = nullable;
        this.sql = sql;
    }

    static Nullable fromISOrules (final String iso) {
        switch (iso) {
            case "YES": return YES;
            case "NO": return NO;
            case "": return UNKNOWN;
            default: throw new IllegalStateException("wrong iso: " + iso);
        }
    }
}
