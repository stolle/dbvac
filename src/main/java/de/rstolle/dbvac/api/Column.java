package de.rstolle.dbvac.api;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Column implements Comparable<Column> {
    private final String name;          // COLUMN_NAME
    private final ColumnType type;      // DATA_TYPE
    private final Integer size;
    private final Nullable nullable;           // IS_NULLABLE
    private IsAutoincrement autoincrement;     // IS_AUTOINCREMENT
    private IsGerneratedColumn generatedColumn;   // IS_GENERATEDCOLUMN
    private final String defaultValue;

    private String columnDef;         // COLUMN_DEF
    private String remarks;           // REMARKS
    private Integer decimalDigits;    // DECIMAL_DIGITS
    private Integer numPrecRadix;     // NUM_PREC_RADIX

    public Column(final String name, final ColumnType type, final Integer size, final Nullable nullable) {
        this(name, type, size, nullable, null);
    }

    public Column(final String name, final ColumnType type, final Integer size,
                  final Nullable nullable, final String defaultValue) {

        Objects.requireNonNull(name);
        Objects.requireNonNull(type);
        Objects.requireNonNull(nullable);

        this.name = name;
        this.type = type;
        this.size = size;
        this.nullable = nullable;
        this.defaultValue = defaultValue;
    }



    public static Column varcharNullable (final String name, int size) {
        return new Column(name, ColumnType.VARCHAR, size, Nullable.YES);
    }

    public static Column varchar (final String name, final int size) {
        return new Column(name, ColumnType.VARCHAR, size, Nullable.NO);
    }

    @Override
    public String toString() {
        return "Column{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", size=" + size +
                ", nullable=" + nullable +
                ", autoincrement=" + autoincrement +
                ", generatedColumn=" + generatedColumn +
                ", defaultValue='" + defaultValue + '\'' +
                ", columnDef='" + columnDef + '\'' +
                ", remarks='" + remarks + '\'' +
                ", decimalDigits=" + decimalDigits +
                ", numPrecRadix=" + numPrecRadix +
                '}';
    }

    public String getName() {
        return name;
    }

    public ColumnType getType() {
        return type;
    }

    public Integer getSize() {
        return size;
    }

    public Nullable getNullable() {
        return nullable;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getColumnDef() {
        return columnDef;
    }

    public void setColumnDef(String columnDef) {
        this.columnDef = columnDef;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(Integer decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

   public void setDecimalDigits(final ResultSet rs) throws SQLException {
        this.decimalDigits = rs.getInt("DECIMAL_DIGITS");
        if (rs.wasNull()) {
            this.decimalDigits=null;
        }
    }

    public Integer getNumPrecRadix() {
        return numPrecRadix;
    }

    public void setNumPrecRadix(Integer numPrecRadix) {
        this.numPrecRadix = numPrecRadix;
    }

    public void setNumPrecRadix (final ResultSet rs) throws SQLException {
        this.numPrecRadix = rs.getInt("NUM_PREC_RADIX");
        if (rs.wasNull()) {
            this.numPrecRadix=null;
        }
    }

    public void setIsAutoincrement (final ResultSet rs) throws SQLException {
        try {
            autoincrement = IsAutoincrement.fromJdbcValue(rs.getString("IS_AUTOINCREMENT"));
        } catch (SQLException e) {
            autoincrement = IsAutoincrement.NOT_SUPPORTED;
        }
    }

    public void setIsGeneratedColumn (final ResultSet rs) throws SQLException {
        try {
            generatedColumn = IsGerneratedColumn.fromJdbcValue(rs.getString("IS_GENERATEDCOLUMN"));
        } catch (SQLException e) {
            generatedColumn = IsGerneratedColumn.NOT_SUPPORTED;
        }
    }


    @Override
    public int compareTo(Column o) {
        return name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Column column = (Column) o;
        return nullable == column.nullable &&
                Objects.equals(name, column.name) &&
                Objects.equals(type, column.type) &&
                Objects.equals(defaultValue, column.defaultValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, nullable, defaultValue);
    }
}
