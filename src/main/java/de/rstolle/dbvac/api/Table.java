package de.rstolle.dbvac.api;

import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

public class Table {
    private final List<Column> columns = new ArrayList<>();
    private final String name;

    public Table(String name) {
        Objects.requireNonNull(name, "name required");
        this.name = name;
    }

    public void add(Column column) {
        Objects.requireNonNull(column, "column required");
        if (columns.contains(column)) {
            throw new IllegalStateException("Try to add column twice: " + column);
        }

        if (getColumn(column.getName()) != null) {
            throw new IllegalStateException("Try to add two columns with the same: " + column);
        }
        columns.add(column);
    }

    public Collection<Column> getColumns() {
        return Collections.unmodifiableCollection(columns);
    }

    public String getName() {
        return name;
    }

    public boolean equalColumns(Table other) {
        return CollectionUtils.isEqualCollection(columns, other.columns);
    }

    public int numColumns() {
        return columns.size();
    }

    public Column getColumn(int idx) {
        return columns.get(idx);
    }

    public Column getColumn(final String name) {
        Objects.requireNonNull(name);
        for (Column col : columns) {
            if (name.equals(col.getName())) {
                return col;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("TABLE " + name + "(\n");

        for (Column c : columns) {
            sb.append("  ").append(c.getName()).append("  ");
            sb.append(c.getType()).append("  ");

            if (c.getSize() != null) {
                sb.append('(').append(c.getSize()).append(")  ");
            }
            sb.append(c.getNullable().sql).append("\n");
        }
        sb.append(")");
        return sb.toString();
    }
}
