package de.rstolle.dbvac.api;

/**
 * A representation for getColumn() -> IS_GENERATEDCOLUMN
 */
public enum IsGerneratedColumn {
    // the first three are as described in jdbc java doc
    YES(true),            // IS_GENERATEDCOLUMN == "YES"
    NO(false),            // IS_GENERATEDCOLUMN == "NO"
    UNKNOWN(null),        // IS_GENERATEDCOLUMN == ""

    UNKNOWN_VALUE(null),  // none of the defined valued
    NOT_SUPPORTED(null);  // exception was thrown trying to select it (H2 1.4.197)

    public final Boolean generated;

    IsGerneratedColumn(final Boolean generated) {
        this.generated = generated;
    }

    static IsGerneratedColumn fromJdbcValue(final String value) {
        switch (value) {
            case "YES":
                return YES;
            case "NO":
                return NO;
            case "":
                return UNKNOWN;
            default:
                // todo log the unknown value "wrong value for IS_GENERATEDCOLUMN: " + value
                return UNKNOWN_VALUE;
        }
    }
}
