package de.rstolle.dbvac.api;

/**
 * Pretty much the same as @see {@link IsGerneratedColumn}
 */
public enum IsAutoincrement {

    YES(true), NO(false), UNKNOWN(null), UNKNOWN_VALUE(null), NOT_SUPPORTED(null);

    public final Boolean autoincrement;

    IsAutoincrement(final Boolean nullable) {
        this.autoincrement = nullable;
    }

    static IsAutoincrement fromJdbcValue(final String value) {
        switch (value) {
            case "YES":
                return YES;
            case "NO":
                return NO;
            case "":
                return UNKNOWN;
            default:
                // logthrow new IllegalStateException("wrong value for IS_AUTOINCREMENT: " + value);
                return UNKNOWN_VALUE;
        }
    }
}
