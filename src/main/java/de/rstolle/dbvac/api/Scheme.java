package de.rstolle.dbvac.api;

import de.rstolle.common.Database;
import de.rstolle.dbvac.config.Databases;
import sun.tools.jconsole.Tab;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class Scheme {

    private static String[] TABLE = new String[] {"TABLE"};
    private static String[] VIEW = new String[] {"VIEW"};
    private static String[] TABLE_VIEW = new String[] {"TABLE", "VIEW"};

    private Set<String> tableTypes = new HashSet<>();
    private Set<Table> tables = new HashSet<>();
    private Set<Table> views = new HashSet<>();

    public Scheme(Database db) {
        try (Connection con = db.getConnection();
             Statement stmt = con.createStatement()) {

            DatabaseMetaData meta = con.getMetaData();
            fetchTableTypes(meta);
            fetchTables(meta);

        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    /**
     *
     * @param meta
     * @throws SQLException
     */
    private void fetchTableTypes (DatabaseMetaData meta) throws SQLException {
        ResultSet results = meta.getTableTypes();
        while (results.next()) {
            String type = results.getString(1);
            if (!tableTypes.add(type)) {
                throw new IllegalStateException("duplicate table type: " + type);
            }
        }

        if (!tableTypes.contains(TABLE[0])) {
            throw new IllegalStateException("No TableType " + TABLE[0] +" found in " + tableTypes);
        }

        if (!tableTypes.contains(VIEW[0])) {
            throw new IllegalStateException("No TableType " + VIEW[0] +" found in " + tableTypes);
        }

        System.out.println("TableTypes: " + tableTypes);
    }

    private void fetchTables (final DatabaseMetaData meta) throws SQLException {
        ResultSet results = meta.getTables(null, null, null, TABLE);
        while (results.next()) {
            String name = results.getString("TABLE_NAME");
            Table tab = new Table(name);
            if (!tables.add(tab)) {
                throw new IllegalStateException("Table '" + name +"' already in scheme");
            }
            fetchColumns(meta, tab);
            System.out.println(tab);
        }
    }

    private void fetchColumns (final DatabaseMetaData meta, final Table tab) throws SQLException {
        final ResultSet rs = meta.getColumns(null, null, tab.getName(), null);
        while (rs.next()) {
            String name = rs.getString("COLUMN_NAME");
            ColumnType type = ColumnType.fromSqlTypes(rs.getInt("DATA_TYPE"));
            Integer size = rs.getInt("COLUMN_SIZE");
            if (rs.wasNull()) {
                size=null;
            }
            Nullable nullable = Nullable.fromISOrules(rs.getString("IS_NULLABLE"));

            Column c = new Column(name, type, size, nullable);

            c.setRemarks(rs.getString("REMARKS"));
            c.setColumnDef(rs.getString("COLUMN_DEF"));
            c.setDecimalDigits(rs);
            c.setNumPrecRadix(rs);
            c.setIsAutoincrement(rs);
            c.setIsGeneratedColumn(rs);

            tab.add(c);

            System.out.println(c);
        }
    }
}
