package de.rstolle.dbvac.config;

import de.rstolle.common.Database;

public enum Databases {

    H2DOS1 (Database.h2Local("d:/res/h2/vac1")),
    H2DOS2 (Database.h2Local("d:/res/h2/vac2")),
    H2UNX1 (Database.h2Local("/java/h2/vac1")),
    H2UNX2 (Database.h2Local("/java/h2/vac2")),
    MARIA_W8STACK(Database.mariaLocal("w8stack", "stolle", "")),
    PG_W8STACK(Database.pgLocal("w8stack", "stolle", ""));

    public final Database database;

    Databases(Database database) {
        this.database = database;
        database.setName(this.toString());
    }
}
