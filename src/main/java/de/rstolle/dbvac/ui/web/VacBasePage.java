package de.rstolle.dbvac.ui.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class VacBasePage extends WebPage {
    public VacBasePage() {
        add(new Label("title", getClass().getSimpleName()));
        add(new Label("overline", "db vac - rocks"));
    }
}
