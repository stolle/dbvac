package de.rstolle.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Defines a data source and some convenient methods to declare and to access.
 */
public class Database {

    public static final String DRV_H2 = "org.h2.Driver";
    public static final String DRV_MARIADB = "org.mariadb.jdbc.Driver";
    public static final String DRV_POSTGRESQL = "org.postgresql.Driver";
    public static final String DRV_MSSS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    private String name = null;
    private final String jdbcDriverClass;
    private final String url;
    private final String catalog;
    private final String user;
    private final String password;

    public Database(String jdbcDriverClass, String url, String catalog, String user, String password) {
        this.jdbcDriverClass = jdbcDriverClass;
        this.url = url;
        this.catalog = catalog;
        this.user = user;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (this.name != null) {
            throw new IllegalStateException("Name can only be set onnce");
        }
        this.name = name;
    }

    public Connection getConnection () throws SQLException {
        try {
            Class.forName(jdbcDriverClass);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Shortcut to create a h2 data source.
     *
     * jdbc:h2:tcp://localhost/d:/res/h2/vac
     * jdbc:h2:tcp://localhost//tmp/h2/vac
     *
     * @param path
     * @return
     */
    public static Database h2Local (final String path) {
        return new Database(DRV_H2, "jdbc:h2:tcp://localhost/" + path, null, "sa", "sa");
    }

    public static Database mariaLocal (final String catalog, final String user, final String password) {
        return new Database(DRV_MARIADB, "jdbc:mariadb://localhost:3306", catalog, user, password);
    }

    public static Database pgLocal (final String db, final String user, final String password) {
        return new Database(DRV_POSTGRESQL, "jdbc:postgresql:" + db, null, user, password);
    }
}



