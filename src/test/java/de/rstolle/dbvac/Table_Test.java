package de.rstolle.dbvac;

import de.rstolle.dbvac.api.Column;
import de.rstolle.dbvac.api.ColumnType;
import de.rstolle.dbvac.api.Nullable;
import de.rstolle.dbvac.api.Table;
import org.junit.Test;

import static org.junit.Assert.*;

public class Table_Test {


    @Test
    public void add() {
        Table tab = new Table("Costumer");
        assertEquals(0, tab.numColumns());

        final Column idCol = new Column("id", ColumnType.BIGINT, null, Nullable.NO);

        tab.add(idCol);
        tab.add(Column.varchar("name", 128));

        assertEquals(2, tab.numColumns());

        assertEquals(idCol, tab.getColumn(0));
        assertEquals(idCol, tab.getColumn("id"));

    }

    @Test
    public void equalColumns() {
        Table c1 = TestData.createCostumer();
        Table c2 = TestData.createCostumer();

        assertTrue(c1.equalColumns(c2));

    }
}