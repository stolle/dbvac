package de.rstolle.dbvac;

import de.rstolle.dbvac.api.Column;
import de.rstolle.dbvac.api.ColumnType;
import de.rstolle.dbvac.api.Nullable;
import de.rstolle.dbvac.api.Table;

import static org.junit.Assert.assertEquals;

public class TestData {


    public static Table createCostumer () {
        Table tab = new Table("Costumer");
        tab.add(new Column("id", ColumnType.BIGINT, null, Nullable.NO));
        tab.add(Column.varchar("name", 64));
        tab.add(Column.varcharNullable("address", 128));
        return tab;
    }

}
